# friendly-octo-umbrella.-
Rich cards are a good way to provide data to Google Search about events, products, or opportunities on your website. Rich card data can be displayed to users in a variety of formats on different devices, and can help drive traffic to your website.
